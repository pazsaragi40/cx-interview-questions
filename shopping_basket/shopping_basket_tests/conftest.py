from ..catalogue import Catalogue
from ..data import catalogue
import pytest


@pytest.fixture()
def _inject_catalogue():
    """
    Injects the Catalogue into a test.
    """
    injected_catalogue = Catalogue(catalogue)
    catalogue_items = injected_catalogue.products
    return [injected_catalogue, catalogue_items]
