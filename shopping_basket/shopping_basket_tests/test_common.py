import pytest
from ..common import nearest_two_decimal_places, non_negative


class TestCommon:
    """
    Unit tests for common functions.
    """
    @pytest.mark.happy_path
    def test_rounds_to_nearest_decimal_place(self):
        assert 2.97 == nearest_two_decimal_places(2.9666)

    @pytest.mark.happy_path
    def test_rounds_to_nearest_scenario_two(self):
        assert 2.94 == nearest_two_decimal_places(2.944)
    
    @pytest.mark.happy_path
    def test_non_negative(self):
        assert 0 == non_negative(-19.76)

    @pytest.mark.unhappy_path
    def test_non_negative_unhappy(self):
        assert 19.76 == non_negative(19.76)

    @pytest.mark.unhappy_path
    def test_non_negative_bad_input(self):
        assert 0 == non_negative(-0)

    @pytest.mark.unhappy_path
    def test_non_negative_null(self):
        assert 0 == non_negative(None)