import pytest
from typing import List
from ..shopping_basket import BasketItem, ShoppingBasket


class TestShoppingBasketIntegrationTests:
    """
    Test suite for Shopping Basket component.
    """
    @pytest.mark.happy_path
    def test_scenario_one(self, _inject_catalogue):
        """
        Basket 1
        Baked Beans x 4
        Biscuits x 1
        Should give the results:

        I think the assignment markdown is incorrect as buy 2 get 1 free means 
        buying 4 beans gives 2 free - so 0.99 * 2 - subtotal.
        """
        [_, catalogue_items] = _inject_catalogue
        basket_items: List[BasketItem] = [
            {
                **catalogue_items[0],
                "quantity": 4,
            },
            {
                **catalogue_items[1],
                "quantity": 1,
            },
        ]

        shopping_basket = ShoppingBasket(basket_items=basket_items)
        shopping_basket.calculate_total_workflow()

        EXPECTED_SUB_TOTAL = 5.16
        EXPECTED_DISCOUNT = 1.98
        EXPECTED_TOTAL = 3.18

        assert shopping_basket.get_subtotal() == EXPECTED_SUB_TOTAL
        assert shopping_basket.get_total_discount() == EXPECTED_DISCOUNT
        assert shopping_basket.get_total() == EXPECTED_TOTAL

    @pytest.mark.unhappy_path
    def test_scenario_one_unhappy(self, _inject_catalogue):
        """
        Basket 1
        Baked Beans x 8
        Biscuits x 1
        """
        [_, catalogue_items] = _inject_catalogue
        basket_items: List[BasketItem] = [
            {
                **catalogue_items[0],
                "quantity": 8,
            },
            {
                **catalogue_items[1],
                "quantity": 1,
            },
            {
                **catalogue_items[4],
                "quantity": 5,
            },
        ]

        shopping_basket = ShoppingBasket(basket_items=basket_items)
        shopping_basket.calculate_total_workflow()

        EXPECTED_SUB_TOTAL = 21.62
        EXPECTED_DISCOUNT = 3.96
        EXPECTED_TOTAL = 17.66

        assert shopping_basket.get_subtotal() == EXPECTED_SUB_TOTAL
        assert shopping_basket.get_total_discount() == EXPECTED_DISCOUNT
        assert shopping_basket.get_total() == EXPECTED_TOTAL

    @pytest.mark.happy_path
    def test_scenario_two(self, _inject_catalogue):
        """
        Baked Beans x 2
        Biscuits x 1
        Sardines x 2
        Should give:

        sub-total: £6.96
        discount: £0.95
        total: £6.01

        1 free beans is 0.99 off the subtotal and 25% off 2 x 1.89 sardines 
        is ~ 0.95 = 1.94.
        """
        [_, catalogue_items] = _inject_catalogue
        basket_items: List[BasketItem] = [
            {
                **catalogue_items[0],
                "quantity": 2,
            },
            {
                **catalogue_items[1],
                "quantity": 1,
            },
            {
                **catalogue_items[2],
                "quantity": 2,
            },
        ]

        shopping_basket = ShoppingBasket(basket_items=basket_items)
        shopping_basket.calculate_total_workflow()

        EXPECTED_SUB_TOTAL = 6.96
        EXPECTED_DISCOUNT = 1.94
        EXPECTED_TOTAL = 5.02

        assert shopping_basket.get_subtotal() == EXPECTED_SUB_TOTAL
        assert shopping_basket.get_total_discount() == EXPECTED_DISCOUNT
        assert shopping_basket.get_total() == EXPECTED_TOTAL

    @pytest.mark.unhappy_path
    def test_scenario_two_unhappy(self, _inject_catalogue):
        """
        Test that 0 quantities doesn't introduce errors.
        Test odd quantities for buy 2 get 1 free.

        Basket 2
        Baked beans x 3
        Biscuits x 0
        Sardines x 6
        """
        [_, catalogue_items] = _inject_catalogue
        basket_items: List[BasketItem] = [
            {
                **catalogue_items[0],
                "quantity": 3,
            },
            {
                **catalogue_items[1],
                "quantity": 0,
            },
            {
                **catalogue_items[2],
                "quantity": 6,
            },
        ]

        shopping_basket = ShoppingBasket(basket_items=basket_items)
        shopping_basket.calculate_total_workflow()

        EXPECTED_SUB_TOTAL = 14.31
        EXPECTED_DISCOUNT = 3.83
        EXPECTED_TOTAL = 10.48

        assert shopping_basket.get_subtotal() == EXPECTED_SUB_TOTAL
        assert shopping_basket.get_total_discount() == EXPECTED_DISCOUNT
        assert shopping_basket.get_total() == EXPECTED_TOTAL

    
class TestShoppingBasketUnitTests:
    @pytest.mark.happy_path
    def test_basket_can_container_zero_products(self):
        try:
            ShoppingBasket()
        except Exception:
            pytest.fail("Exception raised")

    @pytest.mark.happy_path
    def test_basket_is_mutable(self, _inject_catalogue):
        [_, catalogue_items] = _inject_catalogue
        basket = ShoppingBasket()
        basket._add_product_to_basket(product=catalogue_items[0], quantity=1)
        assert len(basket.basket_items) == 1

    @pytest.mark.happy_path
    def test_empty_basket_zero_values(self):
        basket = ShoppingBasket()
        assert basket.get_subtotal() == 0
        assert basket.get_total_discount() == 0
        assert basket.get_total() == 0

    @pytest.mark.happy_path
    def test_basket_non_negative(self):
        basket = ShoppingBasket()
        basket.set_sub_total(-1.9)
        basket.set_total(-1.9)
        basket.set_total_discount(-1.9)
        assert basket.get_subtotal() == 0
        assert basket.get_total_discount() == 0
        assert basket.get_total() == 0