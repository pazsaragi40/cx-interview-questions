# Installation

## Install WSL

* Follow instructions here: https://docs.microsoft.com/en-us/windows/wsl/install-win10

## Install dependencies

* Docker is the recommended approach to installation

### With Docker

* Follow instructions here: https://docs.docker.com/get-docker/
* (Optional) install Make
```bash
apt-get install build-essential
```
* Once Make is installed run to build the development docker image and mount into your working directory:
```bash
make dev
```
* If you don't want to use Make, then run:
```bash
docker build -t test_env .
docker run -it -v $(PWD):/tests/ test_env /bin/bash
```

### With Poetry

* If you decide not to use Docker then you can install poetry manually
* Run the command below in a shell:

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

* Test installation was successful

```bash
peotry --version
```

## Install dependencies

* Run:

```bash
poetry install

or

make install
```

## Run Tests

* Run:

```bash
poetry run pytest -v shopping_basket_tests/test_common.py
poetry run pytest -v shopping_basket_tests/test_shopping_basket.py

or 

make run_tests
```