"""
Created 06/09/2021

Base shopping basket component. The main interface for the 
basket pricer component.
"""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from .common import nearest_two_decimal_places, non_negative
from .discounts import DiscountApplier
from .catalogue import Product
from typing import List


@dataclass
class BasketItem(Product):
    quantity: int


class AbstractBasket(ABC):
    
    def __init__(self, basket_items: List[BasketItem]):
        self.sub_total = 0
        self.total = 0
        self.total_discount = 0
        self.basket_items = basket_items

    @abstractmethod    
    def _calculate_sub_total():
        """
        The undiscounted cost of items in the basket.
        """
        pass

    @abstractmethod
    def calculate_total_workflow():
        """
        Core workflow to be implemented by subclass.
        
        The final price of goods in the basket once the discount has 
        been applied.
        """
        pass

    @abstractmethod
    def _add_product_to_basket(self, product: Product):
        pass


class ShoppingBasket(AbstractBasket):
    """
    A collection of goods a customer wishes to buy.

    Mutable - products can be added.
    """
    def __init__(self, basket_items=[]):
        super().__init__(basket_items)
        self.discounter = DiscountApplier()

    def get_total(self):
        return self.total

    def set_total(self, total: float):
        self.total = non_negative(total)

    def get_total_discount(self):
        return self.total_discount

    def set_total_discount(self, total_discount: float):
        self.total_discount = non_negative(total_discount)

    def get_subtotal(self):
        return self.sub_total

    def set_sub_total(self, sub_total: float):
        self.sub_total = non_negative(sub_total)

    def _is_basket_empty(self):
        """
        A basket can contain zero or more products.
        """
        if not self.basket_items:
            # An empty basket has a sub-total, discount and total each of zero.
            self.total = 0
            self.sub_total = 0
            self.total_discount = 0
            return True

    def _calculate_sub_total(self):
        """
        Returns the sum of all products in a basket.
        """
        total = 0

        # empty basket return 0
        if self._is_basket_empty():
            return total
        
        for item in self.basket_items:
            total += item["price"] * item["quantity"]

        return nearest_two_decimal_places(total)

    def _calculate_total_discount(self):
        """
        Applies a discount function to a basket item.
        """
        total_discount = 0
        for item in self.basket_items:
            for discounter in item["discounts"]:
                if not discounter:
                    break
                discount_func = self.discounter.get_discount_func(discounter)
                total_discount += discount_func(
                    price=item["price"], 
                    quantity=item["quantity"]
                )
        return nearest_two_decimal_places(total_discount)

    def calculate_total_workflow(self):
        """
        The final price of goods in the basket once the discount has been 
        applied.
        """
        self.set_total_discount(
            self._calculate_total_discount()
        )
        self.set_sub_total(
            self._calculate_sub_total()
        )
        self.set_total(
            nearest_two_decimal_places(
                self.get_subtotal() - self.get_total_discount()
                )
        )

    def _add_product_to_basket(self, product: Product, quantity: int):
        """
        A basket is mutable - that is to say products an be added to it.
        """
        return self.basket_items.append({
            **product,
            "quantity": quantity
        })