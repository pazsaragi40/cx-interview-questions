from .catalogue import Product
from .discounts import CurrentDiscounts
from typing import List


catalogue: List[Product] = [
    {
        "name": "Baked Beans",
        "price": 0.99,
        "discounts": [CurrentDiscounts.BUY_TWO_GET_ONE_FREE]
    },
    {
        "name": "Biscuits",
        "price": 1.20,
        "discounts": []
    },
    {
        "name": "Sardines",
        "price": 1.89,
        "discounts": [CurrentDiscounts.TWENTY_FIVE_OFF]
    },
    {
        "name": "Shampoo (Small)",
        "price": 2.00,
        "discounts": []
    },
    {
        "name": "Shampoo (Medium)",
        "price": 2.50,
        "discounts": []
    },
    {
        "name": "Shampoo (Large)",
        "price": 3.50,
        "discounts": []
    },
]