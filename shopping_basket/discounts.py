from enum import Enum
from .common import nearest_two_decimal_places


class CurrentDiscounts(Enum):
    BUY_TWO_GET_ONE_FREE = "BUY_TWO_GET_ONE_FREE"
    TWENTY_FIVE_OFF = "TWENTY_FIVE_OFF"


class DiscountApplier:
    """
    Handles business logic for determining discounts. 

    Returns the total discount for a given list of products.
    """

    def get_discount_func(self, discount: CurrentDiscounts):
        """
        Maps discount to discount func 
        """
        if discount == CurrentDiscounts.BUY_TWO_GET_ONE_FREE:
            return self.buy_two_get_one_free_same_product
        elif discount == CurrentDiscounts.TWENTY_FIVE_OFF:
            return self.twenty_five_percent_off_same_product
        else:
            return None

    @staticmethod
    def _count_free_items(divisible_by: int, quantity: int):
        """
        Returns the total free items. Used as a generic method
        for buy x get y free.
        """
        return quantity // divisible_by
    
    def buy_two_get_one_free_same_product(
        self, 
        price: float, 
        quantity: int,
    ) -> float:
        """
        For same type products only.
        """
        free_items = self._count_free_items(2, quantity)
        discount_total = free_items * price
        return nearest_two_decimal_places(discount_total)
    
    def twenty_five_percent_off_same_product(
        self, 
        price: float, 
        quantity: int,
    ) -> float:
        total = price * quantity
        return total * 0.25