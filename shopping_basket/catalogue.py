from dataclasses import dataclass
from .discounts import CurrentDiscounts
from typing import List, Union


@dataclass
class Product:
    name: str
    price: float
    discounts: Union[List[CurrentDiscounts], List]


@dataclass
class Catalogue:
    """
    Catalogue and offers are maintained by two different teams. 
    It's possible that there are offers on products which are no 
    longer in the catalogue. It's also possible that there are items 
    in the catalogue with no offers, or multiple offers.
    """
    products: List[Product]