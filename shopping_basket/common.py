def nearest_two_decimal_places(price: float):
    if not price:
        return 0
    return round(price, 2)


def non_negative(value: float):
    if not value:
        return 0
    return max(0, value)